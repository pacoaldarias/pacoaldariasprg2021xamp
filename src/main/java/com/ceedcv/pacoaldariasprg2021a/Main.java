/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceedcv.pacoaldariasprg2021a;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Main {

   /**
    * @param args the command line arguments
    */
   public static void main(String[] args) {
      // TODO code application logic here

      try {
// Cargamos la clase que implementa el Driver
         try {
            System.out.println("Conectando a la base de datos...");
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            //Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
// Creamos una nueva conexión a la base de datos 'prueba'
            String url = "jdbc:mysql://172.22.0.2:3306/myDb?serverTimezone=UTC";
            Connection conn = DriverManager.getConnection(url, "user", "test");
// Obtenemos un Statement de la conexión
            java.sql.Statement st = conn.createStatement();
// Ejecutamos una consulta SELECT para obtener la tabla vendedores
            String sql = "SELECT * FROM Person";
            ResultSet rs = st.executeQuery(sql);
// Recorremos todo el ResultSet y mostramos sus datos
            while (rs.next()) {
               int id = rs.getInt("id");
               String nombre = rs.getString("name");

               System.out.println(id + " " + nombre);
            }
// Cerramos el statement y la conexión
            st.close();
            conn.close();
         } catch (SQLException e) {
            e.printStackTrace();
         }
      } catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
         ex.printStackTrace();

      }

   }

}
