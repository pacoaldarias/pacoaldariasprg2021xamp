SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


CREATE TABLE `Person` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `Person` (`id`, `name`) VALUES
(1, 'William'),
(2, 'Marc'),
(3, 'John');


CREATE TABLE cliente (
  id serial NOT NULL ,
  dni varchar(60)  NOT NULL,
  Nombre varchar(50) NOT NULL DEFAULT 0,
  Apellido varchar(50) NOT NULL DEFAULT 0,
  Correo varchar(50)  NOT NULL,
  Telefono varchar(60) NOT NULL,
  usuario varchar(45) DEFAULT NULL,
  password varchar(45) DEFAULT NULL
);

INSERT INTO cliente (id, dni, Nombre, Apellido, Correo, Telefono, usuario, password) VALUES
(1, '45567889J', 'Frank', 'Riesco', 'frankrm@yahoo.es', '655123456', 'frank', '12345'),
(2, '44898779Y', 'Paco', 'Aldarias', 'paco@yahoo.es', '655555555', 'paco', '12345'),
(3, '23878974T', 'Juan', 'De la Cruz', 'juancruz@gmail.com', '654578900', 'juan', '12345');

ALTER TABLE cliente
  ADD PRIMARY KEY (id);


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
